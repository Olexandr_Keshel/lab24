﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab24CSharp
{
    public partial class Form1 : Form
    {
        bool goLeft;
        bool goRight;
        bool isGameOver;

        int score;
        int ballX;  //ball speed in the horizontal direction
        int ballY;  //ball speed in the vertical direction
        int playerSpeed;

        PictureBox[] blockArray; 

        Random rnd = new Random();

        
        public Form1()
        {
            InitializeComponent();
            PlaceBlocks();
        }

        private void SetUpGame()
        {
            isGameOver = false;
            score = 0;
            ballX = 5;  
            ballY = 5;  
            playerSpeed = 12;

            //default location of the player's platform
            player.Left = 342;
            player.Top = 491;

            //default location of the ball
            ball.Left = 375;
            ball.Top = 248;


            gameTimer.Start();

            foreach (Control el in this.Controls)
            {
                if (el is PictureBox && (string)el.Tag == "blocks")
                {
                    el.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                }
            }
        }


        private void PlaceBlocks()
        {
            blockArray = new PictureBox[15];

            int colsInRow = 0;
            int top = 70;
            int left = 80;


            for(int i = 0; i < blockArray.Length; i++)
            {
                blockArray[i] = new PictureBox();
                blockArray[i].Height = 32;
                blockArray[i].Width = 100;
                blockArray[i].Tag = "blocks";

                if (colsInRow < 5)
                {
                    colsInRow++;
                    blockArray[i].Left = left;
                    blockArray[i].Top = top;
                    this.Controls.Add(blockArray[i]);

                    left = left + blockArray[i].Width + 30;
                }

                if(colsInRow == 5)
                {
                    top += 70;
                    left = 80;
                    colsInRow = 0;

                }

                SetUpGame();

            }
        }
        private void GameOver(string message)
        {
            isGameOver = true;
            gameTimer.Stop();

            scoreLabel.Text = "Score: " + score + " " + message;
        }
        private void RemoveRemainingBlocks()
        {
            foreach(PictureBox block in blockArray)
            {
                this.Controls.Remove(block);
            }
        }
        private void MainGameTimerEvent(object sender, EventArgs e)
        {
            scoreLabel.Text = "Score: " + score;

            // movement of the  player's platform, which cannot go beyond of the screen
            if (goLeft == true && player.Left > 0)
            {
                player.Left -= playerSpeed;
            }
            if (goRight == true && player.Right < this.ClientSize.Width)
            {
                player.Left += playerSpeed;
            }

            //movement of the ball
            ball.Left += ballX;
            ball.Top += ballY;

            //if ball hits side or top edge of the form area - it will change its direction 
            if (ball.Left < 0 || ball.Right > this.ClientSize.Width)
            {
                ballX = -ballX;
            }
            if (ball.Top < 0)
            {
                ballY = -ballY;
            }

            // when ball collides with player's platform - ball'll change its horrizontal direction and will get random speed
            if (ball.Bounds.IntersectsWith(player.Bounds))
            {
                ballY = rnd.Next(5, 10) * -1;

                if (ballX < 0)
                {
                    ballX = rnd.Next(5, 10) * -1;
                }
                else
                {
                    ballX = rnd.Next(5, 10);
                }
            }

            // when ball collides with blocks
            foreach (Control el in this.Controls)
            {
                if (el is PictureBox && (string)el.Tag == "blocks")
                {
                    if (ball.Bounds.IntersectsWith(el.Bounds))
                    {
                        score += 1;
                        ballY = -ballY;

                        this.Controls.Remove(el);
                    }
                }
            }

            if(score == 15)
            {
                GameOver("You Win!!!  Press ENTER to play again.");
            }
            if (ball.Bottom > this.ClientSize.Height)
            {
                GameOver("You Lose!!! Press ENTER to try again.");
            }


        }
        private void KeyIsDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Left)
            {
                goLeft = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = true;
            }

        }

        private void KeyIsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                goLeft = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                goRight = false;
            }
            if(e.KeyCode == Keys.Enter && isGameOver == true)
            {
                RemoveRemainingBlocks();
                PlaceBlocks();

            }
        }
    }
}
